# Changelog

## Unreleased

### Fixed
- #26 Ajouter un saut de ligne avant le suffixe pour éviter qu'il ne soit collé au texte

### Changed
- #27 Plus de discernement dans l'erreur de création d'un nouveau compte

## V2.2.0 - 2025-01-20 #HelloQuitX 
- mise à jour librairie : sécurité + se connecter à d'autres applis que mastodon

## V2.1.4 - 2024-12-24

### Fixed
 - rétablir la compat SPIP 3.2 supprimée par erreur

## V2.1.2 - 2024-12-11

### Fixed
 - #10 La syntaxe d'une boucle (DATA){source table nécessite une virgule
 - #11 Éviter indéfinition à la première notification de publication d'un article

## v2.1.1 - 2024-10-14

### Added

 - #1 Préfixer/suffixer le contenu des messages postés
 - Ajout d'un CHANGELOG.md

### Fixed
 - #6 Remplacement des boucles POUR par des boucles DATA 

## v2.1.0 - 2023-07-05

 ### Changed
 - Compatibilité maximum 4
 [...]